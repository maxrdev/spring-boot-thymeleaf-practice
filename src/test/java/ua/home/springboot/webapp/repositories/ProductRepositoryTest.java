package ua.home.springboot.webapp.repositories;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.home.springboot.webapp.model.Product;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    @Before
    public void setUp() throws Exception {
        Product product = new Product();
        product.setName("Spring Framework Guru Shirt");
        product.setDescription("Spring Framework Guru Shirt");
        product.setPrice(new BigDecimal("18.95"));
        product.setImageUrl("https://springframework.guru/wp-content/uploads/2015/04/spring_framework_guru_shirt-rf412049699c14ba5b68bb1c09182bfa2_8nax2_512.jpg");
        product.setSku("235268845711068308");
        entityManager.persist(product);
        entityManager.flush();

        Product product1 = new Product();
        product1.setName("Test Name");
        product1.setDescription("Spring Framework Guru Shirt");
        product1.setPrice(new BigDecimal("18.95"));
        product1.setImageUrl("https://springframework.guru/wp-content/uploads/2015/04/spring_framework_guru_shirt-rf412049699c14ba5b68bb1c09182bfa2_8nax2_512.jpg");
        product1.setSku("235268845711068308");
        entityManager.persist(product1);
        entityManager.flush();
    }

    //Useless test, because method findByName doesn't use, but was added as practice
    @Test
    public void whenFindByName_thenReturnProduct() {
        String searchName = "Spring Framework Guru Shirt";
        Product found = this.productRepository.findByName(searchName);
        assertThat( found.getName(), is(searchName));
    }

    @Test
    public void findAllByName() throws Exception {
        String searchName = "Test Name";
        Page<Product> product = this.productRepository.findAllByName(searchName, new PageRequest(0, 10));
        assertThat( product.iterator().next().getName(), is(searchName));
    }
}