package ua.home.springboot.webapp.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"ua.home.springboot.webapp.model"})
@EnableJpaRepositories(basePackages = {"ua.home.springboot.webapp.repositories"})
@EnableTransactionManagement
public class RepositoryConfiguration {
}
