package ua.home.springboot.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ua.home.springboot.webapp.services.UserService;


@Controller
public class UserController {

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/user/list")
    public String list(Model model) {
        model.addAttribute("users", userService.list());
        return "users";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/user/show/{username}")
    public String get(@PathVariable String username, Model model) {
        model.addAttribute("user", userService.findByUsername(username));
        return "usershow";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/user/show")
    public String get(@RequestParam("id") Integer id, Model model) {
        model.addAttribute("user", userService.getById(id));
        return "usershow";
    }
}
