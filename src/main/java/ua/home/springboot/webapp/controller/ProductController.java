package ua.home.springboot.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.home.springboot.webapp.model.Product;
import ua.home.springboot.webapp.model.User;
import ua.home.springboot.webapp.services.ProductService;
import ua.home.springboot.webapp.utils.PageWrapper;

import javax.validation.Valid;

@Controller
public class ProductController {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "product/new")
    public String create(Model model) {
        model.addAttribute("product", new Product());
        return "productform";
    }

    @RequestMapping(value = "product", method = RequestMethod.POST)
    public String save(@Valid Product product, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "productform";
        }
        productService.save(product);
        return "redirect:/product/" + product.getId();
    }

    @RequestMapping(value = "product/{id}")
    public String get(@PathVariable Integer id, Model model) {
        model.addAttribute("product", productService.getProductById(id));
        return "productshow";
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "products", method = RequestMethod.GET)
    public String list(Model model, Pageable pagebale) {
        PageWrapper<Product> pageWrapper = new PageWrapper<Product>(productService.listByPage(pagebale), "/products");
        model.addAttribute("products", pageWrapper.getContent());
        model.addAttribute("page", pageWrapper);
        return "products";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "product/update/{id}")
    public String update(@PathVariable Integer id, Model model) {
        model.addAttribute("product", productService.getProductById(id));
        return "productform";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping("product/delete/{id}")
    public String delete(@PathVariable Integer id) {
        productService.delete(id);
        return "redirect:/products";
    }

    @RequestMapping(value = "/products/search", method = RequestMethod.POST)
    public  String find(String name, Model model, Pageable pageable) {
        PageWrapper<Product> pageWrapper = new PageWrapper<Product>(productService.findAllByNameOrDescription(name, pageable), "/products");
        model.addAttribute("products", pageWrapper.getContent());
        model.addAttribute("page", pageWrapper);
        return "products";
    }
}
