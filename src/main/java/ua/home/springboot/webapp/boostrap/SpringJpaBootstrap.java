package ua.home.springboot.webapp.boostrap;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ua.home.springboot.webapp.model.Product;
import ua.home.springboot.webapp.model.Role;
import ua.home.springboot.webapp.model.User;
import ua.home.springboot.webapp.repositories.ProductRepository;
import ua.home.springboot.webapp.services.RoleService;
import ua.home.springboot.webapp.services.UserService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component
public class SpringJpaBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private ProductRepository productRepository;

    private RoleService roleService;
    private UserService userService;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        loadProducts();
        loadUsers();
        loadRoles();
        assignUsersToUserRole();
        assignUsersToAdminRole();
        assignUserToProduct();
    }

    private void loadProducts() {

        Faker faker = new Faker();

        for (int i = 0; i < 100; i++) {
            Product product = new Product();
            product.setName(faker.book().title());
            product.setDescription(faker.lorem().sentence());
            product.setPrice(new BigDecimal(faker.number().randomDouble(2, 10, 100)));
            product.setImageUrl(faker.internet().image());
            product.setSku(faker.idNumber().valid());
            productRepository.save(product);
        }
    }

    private void loadUsers() {
        User user1 = new User();
        user1.setUsername("user");
        user1.setPassword("user");
        user1.setCreated(new Date());
        user1.setUpdated(new Date());
        userService.saveOrUpdate(user1);

        User user2 = new User();
        user2.setUsername("admin");
        user2.setPassword("admin");
        user2.setCreated(new Date());
        user2.setUpdated(new Date());
        userService.saveOrUpdate(user2);

        User user3 = new User();
        user3.setUsername("userr");
        user3.setPassword("userr");
        user3.setCreated(new Date());
        user3.setUpdated(new Date());
        userService.saveOrUpdate(user3);

    }

    private void loadRoles() {
        Role role = new Role();
        role.setRole("USER");
        role.setCreated(new Date());
        roleService.saveOrUpdate(role);
        Role adminRole = new Role();
        adminRole.setRole("ADMIN");
        roleService.saveOrUpdate(adminRole);
    }
    private void assignUsersToUserRole() {
        List<Role> roles = (List<Role>) roleService.list();
        List<User> users = (List<User>) userService.list();

        roles.forEach(role -> {
            if (role.getRole().equalsIgnoreCase("USER")) {
                users.forEach(user -> {
                    if (!user.getUsername().equals("admin")) {
                        user.addRole(role);
                        userService.saveOrUpdate(user);
                    }
                });
            }
        });
    }
    private void assignUsersToAdminRole() {
        List<Role> roles = (List<Role>) roleService.list();
        List<User> users = (List<User>) userService.list();

        roles.forEach(role -> {
            if (role.getRole().equalsIgnoreCase("ADMIN")) {
                users.forEach(user -> {
                    if (user.getUsername().equals("admin")) {
                        user.addRole(role);
                        userService.saveOrUpdate(user);
                    }
                });
            }
        });
    }

    private void assignUserToProduct() {
        List<User> users = userService.findUsersByRole("USER");
        List<Product> products = (List<Product>) productRepository.findAll();
        Random random = new Random();

        products.forEach(product -> {
            int randonIndex = random.nextInt(users.size());
            User user = users.get(randonIndex);
            product.setUser(user);
            productRepository.save(product);
        });
    }
}
