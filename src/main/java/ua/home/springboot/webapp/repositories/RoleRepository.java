package ua.home.springboot.webapp.repositories;

import org.springframework.data.repository.CrudRepository;
import ua.home.springboot.webapp.model.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {
}
