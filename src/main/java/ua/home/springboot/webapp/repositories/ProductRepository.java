package ua.home.springboot.webapp.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import ua.home.springboot.webapp.model.Product;

public interface ProductRepository extends PagingAndSortingRepository<Product, Integer> {

    @Query("select p from  Product p where p.name like %?1% or p.description like %?1%")
    public Page<Product> findAllByName(String name, Pageable pageable);

    public Product findByName(String name);
}
