package ua.home.springboot.webapp.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ua.home.springboot.webapp.model.Role;
import ua.home.springboot.webapp.model.User;

import java.security.SecureRandom;
import java.util.List;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findByUsername(String username);

    @Query("select u from User u join u.roles r where r.role = :rolename")
    List<User> findUsersByRole(@Param("rolename") String rolename);
}
