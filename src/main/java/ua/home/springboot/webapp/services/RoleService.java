package ua.home.springboot.webapp.services;

import ua.home.springboot.webapp.model.Role;

public interface RoleService extends CrudService<Role> {
}
