package ua.home.springboot.webapp.services;

import ua.home.springboot.webapp.model.Role;
import ua.home.springboot.webapp.model.User;

import java.util.List;

public interface UserService extends CrudService<User> {

    User findByUsername(String username);

    List<User> findUsersByRole(String role);
}
