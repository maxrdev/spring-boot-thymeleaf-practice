package ua.home.springboot.webapp.services;

import java.util.List;

public interface CrudService<T> {

    List<T> list();

    T getById(Integer id);

    T saveOrUpdate(T t);

    void delete(Integer id);
}
