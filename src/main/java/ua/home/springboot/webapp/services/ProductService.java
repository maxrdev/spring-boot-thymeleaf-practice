package ua.home.springboot.webapp.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.home.springboot.webapp.model.Product;

public interface ProductService {

    Product getProductById(Integer id);

    Iterable<Product> list();

    Product save(Product product);

    void delete(Integer id);

    Page<Product> listByPage(Pageable pageable);

    Page<Product> findAllByNameOrDescription(String name, Pageable pageable);
}
