package ua.home.springboot.webapp.services;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EncryptionServiceImpl implements EncryptionService {

    private StrongPasswordEncryptor passwordEncryptor;

    @Autowired
    public void setPasswordEncryptor(StrongPasswordEncryptor passwordEncryptor) {
        this.passwordEncryptor = passwordEncryptor;
    }

    @Override
    public String encryptString(String input) {
        return passwordEncryptor.encryptPassword(input);
    }

    @Override
    public boolean checkPassword(String plainPassord, String encryptPassword) {
        return passwordEncryptor.checkPassword(plainPassord, encryptPassword);
    }
}
