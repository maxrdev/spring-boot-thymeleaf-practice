package ua.home.springboot.webapp.services;

public interface EncryptionService {

    String encryptString(String input);
    boolean checkPassword(String plainPassord, String encryptPassword);
}
