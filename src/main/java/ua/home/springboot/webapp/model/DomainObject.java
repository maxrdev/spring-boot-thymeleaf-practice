package ua.home.springboot.webapp.model;

public interface DomainObject {

    Integer getId();

    void setId(Integer id);

}